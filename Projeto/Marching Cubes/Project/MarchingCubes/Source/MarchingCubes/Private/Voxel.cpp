// Fill out your copyright notice in the Description page of Project Settings.

#include "MarchingCubes.h"
#include "Voxel.h"

// ----- DEFAULT METHODS ----- //

Voxel::Voxel(FVector pos, float s, int f, float iso)
{
	position = pos;
	size = s;
	function = f;
	isoLevel = iso;
}

Voxel::~Voxel()
{
}

// ----- GETS ----- //

FVector Voxel::getPosition()
{
	return position;
}

bool Voxel::hasSomething() {
	return hasTriangles;
}

TArray<FVector> Voxel::getVertices()
{
	return vertices;
}

TArray<FVector> Voxel::getNewVertices()
{
	return newVertices;
}

TArray<int32> Voxel::getTriangles()
{
	return triangles;
}

// ----- OTHER METHODS ----- //

void Voxel::calculateVertices()
{
	float x = position.X;
	float y = position.Y;
	float z = position.Z;

	float half_size = size * 0.5f;

	// UE_LOG(LogTemp, Warning, TEXT("[GENERATOR] Calculating vertices ..."));

	// Vertice 0
	vertices.Add(FVector(x - half_size, y - half_size, z - half_size));
	// Vertice 1
	vertices.Add(FVector(x + half_size, y - half_size, z - half_size));
	// Vertice 2
	vertices.Add(FVector(x + half_size, y + half_size, z - half_size));
	// Vertice 3
	vertices.Add(FVector(x - half_size, y + half_size, z - half_size));
	// Vertice 4
	vertices.Add(FVector(x - half_size, y - half_size, z + half_size));
	// Vertice 5
	vertices.Add(FVector(x + half_size, y - half_size, z + half_size));
	// Vertice 6
	vertices.Add(FVector(x + half_size, y + half_size, z + half_size));
	// Vertice 7
	vertices.Add(FVector(x - half_size, y + half_size, z + half_size));

	// for (int i = 0; i < 8; i++)
	//	  UE_LOG(LogTemp, Warning, TEXT("[GENERATOR] Position[%d] = %.2f %.2f %.2f"), i, vertices[i].X, vertices[i].Y, vertices[i].Z);
}

void Voxel::calculateFunction()
{
	// UE_LOG(LogTemp, Warning, TEXT("[GENERATOR] Calculating function ..."));

	values.Init(0.0f, 8);

	for (int i = 0; i < 8; i++) {
		switch (function) {
			case 1:
				values[i] = sphereFunction(vertices[i]);
				break;
			case 2:
				values[i] = torusFunction(vertices[i]);
				break;
			case 3:
				values[i] = rippleFunction(vertices[i]);
				break;
			case 4:
				values[i] = tubeFunction(vertices[i]);
				break;
			case 5:
				values[i] = bumpsFunction(vertices[i]);
				break;
			case 6:
				values[i] = randomFunction1(vertices[i]);
				break;
			default:
				values[i] = sphereFunction(vertices[i]);
		}
		// UE_LOG(LogTemp, Warning, TEXT("[GENERATOR] Value[%d] = %.2f"), i, values[i]);
	}
}

bool Voxel::polygonize()
{
	int cubeIndex;
	int newVertexCount = 0;
	int triangleCount = 0;

	FVector vertexList[12];
	FVector newVertexList[12];
	int localRemap[12];

	// Determine the index into the edge table which
	// Tells us which vertices are inside of the surface
	cubeIndex = 0;

	if (values[0] < isoLevel) cubeIndex |= 1;
	if (values[1] < isoLevel) cubeIndex |= 2;
	if (values[2] < isoLevel) cubeIndex |= 4;
	if (values[3] < isoLevel) cubeIndex |= 8;
	if (values[4] < isoLevel) cubeIndex |= 16;
	if (values[5] < isoLevel) cubeIndex |= 32;
	if (values[6] < isoLevel) cubeIndex |= 64;
	if (values[7] < isoLevel) cubeIndex |= 128;

	// Voxel is entirely in/out of the surface
	if (edgeTable[cubeIndex] == 0) {
		hasTriangles = false;
		return false;
	}

	// Find the vertices where the surface intersects the cube
	if (edgeTable[cubeIndex] & 1)
		vertexList[0] = interpolate(vertices[0], vertices[1], values[0], values[1]);

	if (edgeTable[cubeIndex] & 2)
		vertexList[1] = interpolate(vertices[1], vertices[2], values[1], values[2]);

	if (edgeTable[cubeIndex] & 4)
		vertexList[2] = interpolate(vertices[2], vertices[3], values[2], values[3]);

	if (edgeTable[cubeIndex] & 8)
		vertexList[3] = interpolate(vertices[3], vertices[0], values[3], values[0]);

	if (edgeTable[cubeIndex] & 16)
		vertexList[4] = interpolate(vertices[4], vertices[5], values[4], values[5]);

	if (edgeTable[cubeIndex] & 32)
		vertexList[5] = interpolate(vertices[5], vertices[6], values[5], values[6]);

	if (edgeTable[cubeIndex] & 64)
		vertexList[6] = interpolate(vertices[6], vertices[7], values[6], values[7]);

	if (edgeTable[cubeIndex] & 128)
		vertexList[7] = interpolate(vertices[7], vertices[4], values[7], values[4]);

	if (edgeTable[cubeIndex] & 256)
		vertexList[8] = interpolate(vertices[0], vertices[4], values[0], values[4]);

	if (edgeTable[cubeIndex] & 512)
		vertexList[9] = interpolate(vertices[1], vertices[5], values[1], values[5]);

	if (edgeTable[cubeIndex] & 1024)
		vertexList[10] = interpolate(vertices[2], vertices[6], values[2], values[6]);

	if (edgeTable[cubeIndex] & 2048)
		vertexList[11] = interpolate(vertices[3], vertices[7], values[3], values[7]);

	for (int i = 0; i < 12; i++)
		localRemap[i] = -1;

	// ----- NEW VERTICES ----- //

	for (int i = 0; triTable[cubeIndex][i] != -1; i++)
	{
		if (localRemap[triTable[cubeIndex][i]] == -1)
		{
			newVertexList[newVertexCount] = vertexList[triTable[cubeIndex][i]];
			localRemap[triTable[cubeIndex][i]] = newVertexCount;
			newVertexCount++;
		}
	}

	// PRINT NUMBER OF NEW VERTICES
	// UE_LOG(LogTemp, Warning, TEXT("[GENERATOR] Count NEW VERTICES = %d"), newVertexCount);
	newVertices.Init(FVector(), newVertexCount);

	for (int i = 0; i < newVertexCount; i++) {
		newVertices[i] = newVertexList[i];
		// UE_LOG(LogTemp, Warning, TEXT("[GENERATOR] NewVertices[%d] = %.2f %.2f %.2f"), i, newVertices[i].X, newVertices[i].Y, newVertices[i].Z);
	}

	// ----- TRIANGLES ----- //

	for (int i = 0; triTable[cubeIndex][i] != -1; i += 3) {
		triangleCount++;
	}

	// PRINT NUMBER OF TRIANGLES
	// UE_LOG(LogTemp, Warning, TEXT("[GENERATOR] Count TRIANGLES = %d"), triangleCount);
	triangles.Init(0, triangleCount * 3);

	for (int i = 0; triTable[cubeIndex][i] != -1; i++) {
		triangles[i] = localRemap[triTable[cubeIndex][i]];
		// UE_LOG(LogTemp, Warning, TEXT("[GENERATOR] Triangle[%d] = %d"), i, triangles[i]);
	}

	hasTriangles = true;

	return true;
}

// Linearly interpolate the position where an isosurface cuts an edge between two vertices, each with their own scalar value
FVector Voxel::interpolate(const FVector &p1, const FVector &p2, float valp1, float valp2)
{
	return (p1 + (-valp1 / (valp2 - valp1)) * (p2 - p1));
}

// ----- FUNCTIONS ----- //

float Voxel::sphereFunction(FVector v) {
	return v.X * v.X + v.Y * v.Y + v.Z * v.Z - 16.0f;
}

float Voxel::torusFunction(FVector v) {
	return powf((0.016f - powf((0.6f - powf((v.X * v.X + v.Y * v.Y), 0.5f)), 2.0f)), 0.5f);
}

float Voxel::rippleFunction(FVector v) {
	return sinf(4.0f * (v.X * v.X + v.Y * v.Y)) / 4.0f;
}

float Voxel::tubeFunction(FVector v) {
	return 1.0f / (15.0f * (v.X * v.X + v.Y * v.Y));
}

float Voxel::bumpsFunction(FVector v) {
	return sinf(5.0f * v.X) * cosf(5.0f * v.Y) / 5.0f;
}

float Voxel::randomFunction1(FVector v) {
	return sinf(v.X * v.Y + v.X * v.Z + v.Y * v.Z) + sinf(v.X * v.Y) + sinf(v.Y * v.Z) + sinf(v.X * v.Z) - 1.0f;
}