// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ProceduralMeshComponent.h"

#include "GameFramework/Actor.h"
#include "Mesh.generated.h"

UCLASS()
class MARCHINGCUBES_API AMesh : public AActor
{
	GENERATED_BODY()
	
	UPROPERTY(VisibleAnywhere, Category = Materials)
	UProceduralMeshComponent *mesh;

	// ----- DEFAULT METHODS ----- //

public:	
	// Sets default values for this actor's properties
	AMesh();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

public:
	void setup(TArray<FVector> vertices, TArray<int32> triangles);
};
