// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Voxel.h"
#include "Cell.h"
#include "Mesh.h"

/**
 * 
 */
class MARCHINGCUBES_API Grid
{
	UWorld *world;

	float xmin;
	float xmax;
	float ymin;
	float ymax;
	float zmin;
	float zmax;
	float voxelSize;

	int function;
	float isoLevel;

	TArray<Voxel> voxels;
public:
	Grid();
	Grid(UWorld *world, float xi, float xm, float yi, float ym, float zi, float zm, float vs, int function, float isoLevel);
	~Grid();

	bool generateVoxelPositions();
	void drawFunction();
};
