// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Grid.h"

#include "GameFramework/Actor.h"
#include "Generator.generated.h"

UCLASS()
class MARCHINGCUBES_API AGenerator : public AActor
{
	GENERATED_BODY()

public:	
	// Sets default values for this actor's properties
	AGenerator();

	UFUNCTION(BlueprintCallable, Category = Generator)
	void drawFunction(float xmin, float xmax, float ymin, float ymax, float zmin, float zmax, float voxelSize, int function, float isoLevel);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
