# ----- Rendering Setup -----

# View transform
LookAt 0 -3.4 0 0 1 0 0 0 1

# Perspective camera 45 degrees of Field Of View
Camera "perspective" "float fov" [45]

# Output
Film "image" "string filename" ["cornell.exr"] "integer xresolution" [600] "integer yresolution" [600]

# Ray distribution sampling strategy
Sampler "stratified" "integer xsamples" [8] "integer ysamples" [8] "bool jitter" ["true"]

# SurfaceIntegrator's tell us which light model to use to shade every point
SurfaceIntegrator "directlighting"

# ----- World Description -----

WorldBegin

  Identity

  # Area Light Source
  AttributeBegin

    AreaLightSource "area"
      "color L" [25 25 25]
      "integer nsamples" [4]
    Shape "trianglemesh"
      "integer indices" [2 1 0 2 3 1]
      "point P" [-0.25 -0.25 0.99 0.25 -0.25 0.99 -0.25 0.25 0.99 0.25 0.25 0.99]

  AttributeEnd

  # walls
  AttributeBegin

    # white walls material
    Material "plastic"
      "color Kd" [1 1 1]
      "color Ks" [0.1 0.1 0.1]
      "float roughness" 0.15

    # back wall
    Shape "trianglemesh"
      "integer indices" [0 1 2 2 3 0]
      "point P" [-1 1 -1 -1 1 1 1 1 1 1 1 -1]

    # ceiling
    Shape "trianglemesh"
      "integer indices" [2 1 0 0 3 2]
      "point P" [-1 1 1 1 1 1 1 -1 1 -1 -1 1]

    # floor
    Shape "trianglemesh"
      "integer indices" [0 1 2 2 3 0]
      "point P" [-1 1 -1 1 1 -1 1 -1 -1 -1 -1 -1]

    # red wall material
    Material "plastic"
      "color Kd" [0.8 0.1 0.1]
      "color Ks" [0.1 0.1 0.1]
      "float roughness" 0.15

    # left red wall
    Shape "trianglemesh"
      "integer indices" [0 1 2 2 3 0]
      "point P" [-1 -1 -1 -1 -1 1 -1 1 1 -1 1 -1]

    # blue wall material
    Material "plastic"
      "color Kd" [0.2 0.3 0.8]
      "color Ks" [0.1 0.1 0.1]
      "float roughness" 0.15

    # right blue wall
    Shape "trianglemesh"
      "integer indices" [2 1 0 0 3 2]
      "point P" [1 -1 -1 1 -1 1 1 1 1 1 1 -1]

  AttributeEnd

  # glass sphere
  AttributeBegin

    Translate -0.45 0 -0.1

    # glass material
    Material "glass"
      "color Kr" [0.6 0.6 0.6]
      "color Kt" [0.96 0.96 0.96]
      "float index" [1.5]
    Shape "sphere"
      "float radius" [0.35]

  AttributeEnd

  # metal sphere
  AttributeBegin

    Translate 0.45 0.4 -0.65

    # metal material
    Material "shinymetal"
      "color Ks" [0.7 0.7 0.7]
      "color Kr" [0.8 0.8 0.8]
      "float roughness" [0.02]
    Shape "sphere"
      "float radius" [0.35]

  AttributeEnd

  # mirror
  AttributeBegin

    # mirror material
    Material "mirror"
      "color Kr" [0.9 0.9 0.9]
    # mirror
    Shape "trianglemesh"
      "integer indices" [2 1 0 1 3 2]
      "point P" [0.99 -0.45 0 0.99 0.45 0 0.99 -0.45 -0.9 0.99 0.45 -0.9]

  AttributeEnd

WorldEnd
