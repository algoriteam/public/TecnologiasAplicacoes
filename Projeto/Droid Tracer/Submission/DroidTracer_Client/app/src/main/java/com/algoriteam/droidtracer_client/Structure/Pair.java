package com.algoriteam.droidtracer_client.Structure;

import java.io.Serializable;

public class Pair implements Serializable {

    // ----- VARIABLES -----
    
        private Double first, second;

    // ----- CONSTRUCTOR -----
        
    public Pair(double first, double second) {
        this.first = first;
        this.second = second;
    }

    // ----- GETTERS -----
    
    public Double getFirst() {
        return first;
    }
    
    public Double getSecond() {
        return second;
    }

    // ----- SETTERS -----
    
    public void setFirst(Double first) {
        this.first = first;
    }

    public void setSecond(Double second) {
        this.second = second;
    }
    
}
