package com.algoriteam.droidtracer_client.Presentation;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.text.InputType;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.algoriteam.droidtracer_client.Data.SectionsPagerAdapter;
import com.algoriteam.droidtracer_client.R;

public class Tabs extends AppCompatActivity {

    // ----- VARIABLES -----

    // THREAD
    private Thread work;

    // ----- METHODS -----

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tabs);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Create the adapter that will return a fragment for each of the two primary sections of the activity.
        SectionsPagerAdapter mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        ViewPager mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                AlertDialog.Builder builder = new AlertDialog.Builder(Tabs.this);
                builder.setMessage("Do you really want to exit?");

                // Set up the buttons
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    work = new Thread(new Runnable() {
                        @Override
                        public void run() {
                        showMessage("Disconnecting from server");

                        // DISCONNECT FROM SERVER
                        Menu.server.disconnect(Menu.guid);
                        Menu.server = null;

                        showMessage("Success finalizing server call");
                        finish();
                        }
                    });

                    work.start();
                    }
                });

                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                    }
                });

                builder.show();
                break;
        }
        return true;
    }

    private void showMessage(String text) {
        // Get rootView
        View rootView = findViewById(android.R.id.content);

        Snackbar.make(rootView, text, Snackbar.LENGTH_LONG).setAction("Action", null).show();

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
