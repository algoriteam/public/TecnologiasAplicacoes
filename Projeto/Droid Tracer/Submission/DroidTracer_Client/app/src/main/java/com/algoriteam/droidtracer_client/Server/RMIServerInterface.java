package com.algoriteam.droidtracer_client.Server;

import com.algoriteam.droidtracer_client.Structure.FileTreeNode;
import com.algoriteam.droidtracer_client.Structure.Pair;

import java.io.IOException;

public interface RMIServerInterface {

    // TESTING
    public String helloWorldRemote(String test);

    // CONNECTION
    public boolean connect(String guid);
    public boolean disconnect(String guid);
    
    // RENDERING
    public int getStatus(String guid);
    public Pair getTimeRemaining(String guid);
    public void startRender(String guid, String fileContents) throws IOException;
    public void stopRendering(String guid);
    
    // RESULT
    public byte[] getRenderedImage(String guid) throws IOException;

    // RESOURCES
    public FileTreeNode listResources(String guid);
}
