package com.algoriteam.droidtracer_client.Server;

import com.algoriteam.droidtracer_client.RMI.RpcServer;
import java.rmi.RemoteException;

public class RMIServer implements Runnable {
    
    // ----- VARIABLES -----

    RMIServerImpl rmiServer;
    int defaultPort = 10000;

    // ----- CONSTRUCTOR -----
    
    public RMIServer() {
        rmiServer = new RMIServerImpl();
        
        // Create the RMI server
        RpcServer rpcServer = new RpcServer();
        // Register a service under the name example
        // The service has to implement an interface for the magic to work
        rpcServer.registerService("Tracer", rmiServer);
        // Start the server at port 10000
        rpcServer.start(defaultPort);
    }

    // ----- METHODS -----
    
    @Override
    public void run() {
        while(true){}
    }

    public static void main(String[] args) throws RemoteException {
        new RMIServer().run();
    }
}


