package Example;

/**
 * An example service interface
 * @author h.indzhov
 */
public interface ServiceExample_Interface {
 
	// concatenate the arguments
	public String concat(String... args);

}
