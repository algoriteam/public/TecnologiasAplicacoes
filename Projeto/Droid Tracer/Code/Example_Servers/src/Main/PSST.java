package Main;

import Server.SERVIDOR_RMI;
import Server.SERVIDOR_SOCKET;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

public class PSST {
    
    //--------------------------------------- VARIÁVEIS ------------------------------------------------------
    
    //variáveis privadas logo estao sempre seguras
    private static SERVIDOR_SOCKET socket;
    private static SERVIDOR_RMI rmi;
    
    private static String lista;
    private static ArrayList<String> Log_File;
 
    //--------------------------------------- MAIN ------------------------------------------------------
    
    public static void main(String[] args) throws RemoteException, IOException, InterruptedException {
        socket = new SERVIDOR_SOCKET();
        rmi = new SERVIDOR_RMI();
        Log_File = new ArrayList<>();
        /* Estado inicial da "LISTA" de IP's do servidor HTTP (este 'backup' da lista realizado pelo sistema foi adicionado para o caso de o primeiro utilizador que se liga ao 
           servidor RMI se registar para o CallBack e ainda não existir nenhum estado atualizado da "LISTA" de IP's no sistema) */
        lista = PEDIDO(InetAddress.getLocalHost().getHostAddress());
        if (lista.equals("ERROR")){
            System.out.println("[HTTP-SERVER-ERROR] Erro a escutar a porta 80 de 'heartbeat.dsi.uminho.pt'");
            try {
                Thread.sleep(2000);
            } catch (InterruptedException ex) {
            }
            System.exit(0);
        }
        PSST.addLog("[LISTA] A obter lista de IP's para o servidor");
        Menu();
    }
    
    //--------------------------------------- MÉTODOS ------------------------------------------------------
    
    private static void Menu() throws InterruptedException, IOException{
        Scanner ler = new Scanner(System.in);
        String ESTADO = "";
        char option;
        clearConsole();
        System.out.println("------------------------------------------------\n   ____              _             _");
        System.out.println("  |  _ \\ ___ ___ ___| |_          | |");
        System.out.println("  | |_) / __/ __/ __| __|         | |");
        System.out.println("  |  __/\\__ \\__ \\__ | |_   _ _ _  |_|");
        System.out.println("  |_|   |___|___|___/\\__| (_(_(_) (_) [SERVER]\n");
        System.out.println("          1 - LIGAR\n          2 - DESLIGAR\n          3 - LOGFILE\n");
        System.out.println("  RMI : "+rmi.getStatus()+"\n  SOCKET : "+socket.getStatus()+"\n------------------------------------------------\n                                     Q - Sair\n------------------------------------------------");
        System.out.print("-> Insira opção: ");
        option = ler.nextLine().charAt(0);
        switch(Character.toUpperCase(option)){
            case '1': ESTADO = "LIGAR";
                      break;
            case '2': ESTADO = "DESLIGAR";
                      break;
            case '3': ESTADO = "LOGFILE";
                      break;
            case 'Q': System.exit(0);
                
            default: Menu();
                     break;
        }
        if (ESTADO.equals("LIGAR")) Iniciar_Servidores();
        if (ESTADO.equals("DESLIGAR")) Desligar_Servidores();
        if (ESTADO.equals("LOGFILE")) LogFile();
    }
    
    // Inicia servidores
    private static void Iniciar_Servidores() throws InterruptedException, IOException{
        int port_socket=0;
        int port_rmi=1;
        Scanner ler = new Scanner(System.in);
        
        // Verifica estado dos servidores
        if (socket.getStatus().equals("ONLINE") && rmi.getStatus().equals("ONLINE")){
            System.out.println("[MESSAGE] Servidor já se encontra a correr!");
            try {
                Thread.sleep(2000);
            } catch (InterruptedException ex) {
            }
        }
        if (!socket.getStatus().equals("ONLINE")){
            System.out.print("    -> SOCKET PORT: ");
            try{
                port_socket = ler.nextInt();
            }catch(InputMismatchException | NumberFormatException e){
                Log_File.add("[ERROR] Valor introduzido para PORTA invalido");
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException ex) {
                }
            }
        }
        if (!rmi.getStatus().equals("ONLINE")){
            System.out.print("    -> RMI PORT: ");
            try{
                port_rmi = ler.nextInt();
            }catch(InputMismatchException | NumberFormatException e){
                Log_File.add("[ERROR] Valor introduzido para PORTA invalido");
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException ex) {
                }
            }
        }
            
        // Tratamento do valor das portas introduzidas pelo administrador
        if (port_socket!=port_rmi){
            if (!socket.getStatus().equals("ONLINE")){
                System.out.println("[START] A iniciar servidor Socket ...");
                socket = new SERVIDOR_SOCKET();
                socket.setServer(port_socket, Log_File);
                socket.start();
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException ex) {
                }
            }
            if (!rmi.getStatus().equals("ONLINE")){
                System.out.println("[START] A iniciar servidor RMI ...");
                rmi.setServer(port_rmi);
                Thread sv_rmi = new Thread(rmi);
                sv_rmi.start();
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException ex) {
                }
            }
        }
        if (port_socket==port_rmi){
            Log_File.add("[ERROR] Portas iguais para ambos os servidores");
            System.out.println("[ERROR] Portas escolhidas iguais");
            try {
                Thread.sleep(2000);
            } catch (InterruptedException ex) {
            }
        }
        Menu();
    }
    
    // Desliga servidores
    private static void Desligar_Servidores() throws InterruptedException, IOException{
        if (socket.getStatus().equals("ONLINE")){
            socket.shutdownServer();
            System.out.println("[SHUTDOWN] A desligar servidor socket ...");
            socket.join();
            try {
                Thread.sleep(2000);
            } catch (InterruptedException ex) {
            }
        }
        if (rmi.getStatus().equals("ONLINE")){
            rmi.shutdownServer();
            System.out.println("[SHUTDOWN] A desligar servidor RMI ...");
            try {
                Thread.sleep(2000);
            } catch (InterruptedException ex) {
            }
        }
        Menu();
    }
    
    // Apresenta ao utilizador o LOG_FILE conjunto dos servidores que correm neste sistema
    private static void LogFile() throws InterruptedException, IOException{
        int tam = Log_File.size();
        int inicio = 0; 
        int fim = 0;
        int i = 0;
        Scanner ler = new Scanner(System.in);
        char leitura;
        String ESTADO = "";
        
        while (ESTADO.equals("")){
            for(fim=inicio;fim<inicio+10&&fim<tam;fim++);
            clearConsole();
            System.out.println("------------------------------------------------");
            System.out.println("                                                                                ");
            if (tam==0) System.out.println("      [       *** LOG VAZIO ***        ]");
            else{
                for(i = inicio; i < fim; i++){
                    System.out.println("  "+(i+1)+" - " + Log_File.get(i));
                }
            }
            System.out.println("\n  NºRegistos: "+tam+"\n------------------------------------------------");
            System.out.println("        << (1) | < (2) | (3) > | (4) >>");
            System.out.println("   R - Retroceder");
            System.out.println("------------------------------------------------");
            System.out.print("-> Insira opção : ");
            leitura = ler.next().charAt(0);
            switch (Character.toUpperCase(leitura)) {
                case '1':  inicio = 0;
                           break;
                case '2':  if (inicio-10>=0) inicio-=10;
                           break;
                case '3':  if (inicio+10<tam) inicio+=10;
                           break;
                case '4':  for(inicio=0;inicio+10<tam;inicio+=10);
                           break;
                case 'R':  ESTADO = "MENU";
                           break;
                default :  ESTADO = "";
                           break;
            }
        }
        if (ESTADO.equals("MENU")) Menu();
    } 
    
    //--------------------------------------- MÉTODOS EXTRA ------------------------------------------------------
    
    // Adiciona "LOG" ao LOG_FILE do sistema
    public static void addLog(String a){
        Log_File.add(a);
    }
    
    // Atualiza para a versão mais recente o backup da "LISTA" de IP's que se encontra no sistema
    public static void updateLista(String newlist) throws RemoteException{
        rmi.doCallbacks();
        lista = newlist;
    }
    
    // Devolve "LISTA" de IP's que se encontra neste sistema
    public static String getLista(){
        return lista;
    }
    
    // Limpa o ecrâ da consola
    public static void clearConsole(){
            for(int i = 0; i < 15; i++) System.out.println(); 
    }
    
    //--------------------------------------- PEDIDO CLIENTE-PRESENCAS ------------------------------------------------------
    public static String PEDIDO(String ip) throws IOException  {
            
            /*INFO - http://docs.oracle.com/javase/tutorial/networking/sockets/clientServer.html
              Um SOCKET consiste numa 'ponte de ligação' entre dois programas na mesma rede (Neste caso Cliente <-> Servidor)*/
            
            boolean get_resposta = false;
            boolean ok = true;
            int n_linha=0;
            String hostname = "heartbeat.dsi.uminho.pt";
            String frase;
            StringBuilder resposta = new StringBuilder();
            ArrayList<String> headers = new ArrayList<>();
            
            //Port "80" escolhido porque 'https://pt.wikipedia.org/wiki/Hypertext_Transfer_Protocol#GET'
            try{    
                Socket socketdsi = new Socket(hostname, 80);
                PrintWriter out = new PrintWriter(socketdsi.getOutputStream());
                BufferedReader in = new BufferedReader(new InputStreamReader(socketdsi.getInputStream())); 

                //Pedido GET de acordo com protocolo HTTP
                out.println("GET /heartbeat.svc/show?ip="+ip+" HTTP/1.0\nHost: "+hostname+" \n");

                /*Utilização do flush justifica-se pela necessidade de a mensagem precisar de 'aparecer' logo no servidor após escrita caso contrário fica
                em espera!*/
                out.flush();

                //Resposta do servidor-dsi HTTP
                resposta.append("[LISTA IP]\n\n");
                while((frase=in.readLine())!=null && ok){
                    // Caso de erro de ligação ao servidor do DSI
                    if ((frase.contains("Not Found") || frase.contains("Method Not Allowed"))&&n_linha==1) return "ERROR";
                // 1 -----------------------------------
                    headers.add(frase+"\n");
                    n_linha++;
                }
                resposta.append(headers.get(n_linha-1));
                // --------------------------------------
                
                /* 2 ------------------------------------
                private boolean get_resposta = false;
                ...
                if (get_resposta) resposta.append(frase+"\n");
                if (frase.contains("\n")) ou (frase.equals("\n")) get_resposta = true;
                ---------------------------------------*/ 
                
                
                //HTTP/1.1 200 OKContent-Length: 65Content-Type: text/plainServer: Microsoft-IIS/7.5X-Powered-By: ASP.NETDate: Thu, 26 Nov 2015 23:00:19 GMTConnection: close192.168.1.7;192.168.1.73;169.254.80.80;172.26.40.15;192.168.1.69;
                
                //INFO - http://docs.oracle.com/javase/tutorial/networking/sockets/readingWriting.html

                //Fechar STREAMS
                out.close();
                in.close();

                //Fechar SOCKET
                socketdsi.close();
                
            }catch(IOException e){
                Log_File.add("[HTTP-SERVER-ERROR] Erro a escutar a porta 80 de 'heartbeat.dsi.uminho.pt'");
                return "ERROR";
            }
            
            //Devolve a thread a resposta obtida do servidor-HTTP
            return resposta.toString();
    }
}