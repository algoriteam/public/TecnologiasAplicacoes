import warnings
import prepare_dataset as pd

if __name__ == '__main__':
    with warnings.catch_warnings():
        warnings.simplefilter("ignore")

        d = pd.DATA_PREPARATION()
        (X_train, Y_train), (X_test, Y_test) = d.load_prepare_dataset(True, False, 200, 200, 'jpg', './images/', './classifications/')
